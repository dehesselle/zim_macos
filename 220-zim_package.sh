#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Create the Zim application bundle.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

### variables ##################################################################

SELF_DIR=$(dirname "$(greadlink -f "$0")")

### functions ##################################################################

# Nothing here.

### main #######################################################################

error_trace_enable

zim_download_python

#---------------------------------------------------------------- build launcher

# Build our C++ program to launch Python w/ Zim.
zim_install_python "$TMP_DIR"
(
  if [ "$(uname -m)" = "x86_64" ]; then
    # special treatment on Intel: Build (only) the main binary with a recent SDK
    # (needs to be >=11.x) and set the deployment target to achieve backward
    # compatibility.
    # https://gitlab.gnome.org/GNOME/gtk/-/issues/5305#note_1673947
    MACOSX_DEPLOYMENT_TARGET=$(
      /usr/libexec/PlistBuddy -c \
        "Print DefaultProperties:MACOSX_DEPLOYMENT_TARGET" \
        "$SDKROOT"/SDKSettings.plist
    )
    export MACOSX_DEPLOYMENT_TARGET
    unset SDKROOT
  fi

  g++ \
    -std=c++17 \
    -o "$BIN_DIR"/zimlauncher \
    -I"$TMP_DIR"/Python.framework/Headers \
    -I"$SELF_DIR"/src \
    "$SELF_DIR"/src/zimlauncher.cpp \
    -framework CoreFoundation \
    -F"$TMP_DIR" \
    -framework Python
)
zim_uninstall_python "$TMP_DIR"

#----------------------------------------------------- create application bundle

(
  cd "$SELF_DIR" || exit 1
  export ART_DIR # is referenced in zim.bundle

  jhb run gtk-mac-bundler resources/zim.bundle
)

# remove everything but Zim from lib/pythonN.N
mkdir "$TMP_DIR"/site-packages
find "$ZIM_APP_LIB_DIR/python$ZIM_PYTHON_VER"/site-packages \
  -maxdepth 1 \
  -name "zi*" \
  -exec mv {} "$TMP_DIR"/site-packages \;
rm -rf "$ZIM_APP_LIB_DIR/python$ZIM_PYTHON_VER"
mkdir "$ZIM_APP_LIB_DIR/python$ZIM_PYTHON_VER"
mv "$TMP_DIR"/site-packages "$ZIM_APP_LIB_DIR/python$ZIM_PYTHON_VER"

# install Python.framework into bundle
zim_install_python
rm -rf "$ZIM_APP_LIB_DIR"/python"$ZIM_PYTHON_VER"/test

# set SF Pro font for better kerning
{
  echo -e ""
  cat "$SELF_DIR"/resources/gtk.css
} >> "$ZIM_APP_SHR_DIR"/themes/Mac/gtk-3.0/gtk-keys.css

#------------------------------------------------------- install Zim main script

cp "$BIN_DIR"/zim \
  "$ZIM_APP_LIB_DIR/python$ZIM_PYTHON_VER/site-packages/zim"
chmod 644 \
  "$ZIM_APP_LIB_DIR/python$ZIM_PYTHON_VER/site-packages/zim/zim"

#------------------------------------------------------- install Python packages

zim_pipinstall ZIM_PYTHON_PKG_NUMPY
zim_pipinstall ZIM_PYTHON_PKG_PILLOW
zim_pipinstall ZIM_PYTHON_PKG_PYGOBJECT
zim_pipinstall ZIM_PYTHON_PKG_PYGTKSPELLCHECK
zim_pipinstall ZIM_PYTHON_PKG_XDOT

#--------------------------------------- patch library link paths: Resources/lib

lib_change_siblings "$ZIM_APP_LIB_DIR"

#----------------------------------------------------- patch introspection files

# Add the "@executeble_path/..." prefix to a second library in the
# shared-library list.

grep -n "dylib" "$ZIM_APP_RES_DIR"/share/gir-1.0/*.gir |
    grep "," |
    awk -F":" '{ print $1 }' |
    while IFS= read -r gir; do
  gsed -i -E 's|,(.+\.dylib")|,@executable_path/../Resources/lib/\1|' "$gir"
  jhb run g-ir-compiler \
    -o "$ZIM_APP_LIB_DIR/girepository-1.0/$(basename -s .gir "$gir")".typelib \
    "$gir"
done

#--------------------------------------------- patch library link paths: enchant

lib_change_paths \
  @loader_path/.. \
  "$ZIM_APP_LIB_DIR" \
  "$ZIM_APP_LIB_DIR"/enchant-2/enchant_applespell.so

#-------------------------------------------- patch library link paths: GraphViz

lib_change_paths \
  @executable_path/../lib \
  "$ZIM_APP_LIB_DIR" \
  "$ZIM_APP_BIN_DIR"/dot

lib_change_paths \
  @loader_path/.. \
  "$ZIM_APP_LIB_DIR" \
  "$ZIM_APP_LIB_DIR"/graphviz/*.dylib

if [ -d "$ZIM_APP_LIB_DIR"/graphviz/tcl ]; then
  lib_change_paths \
    @loader_path/../.. \
    "$ZIM_APP_LIB_DIR" \
    "$ZIM_APP_LIB_DIR"/graphviz/tcl/*.dylib
fi

#------------------------------------------------------------- update Info.plist

# enable HiDPI
/usr/libexec/PlistBuddy -c "Add NSHighResolutionCapable bool 'true'" \
  "$ZIM_APP_PLIST"

# enable dark mode (menubar only, GTK theme is reponsible for the rest)
/usr/libexec/PlistBuddy -c "Add NSRequiresAquaSystemAppearance bool 'false'" \
  "$ZIM_APP_PLIST"

# set minimum OS version
/usr/libexec/PlistBuddy -c "Set LSMinimumSystemVersion '$SYS_SDK_VER'" \
  "$ZIM_APP_PLIST"

# set Zim version
/usr/libexec/PlistBuddy -c \
  "Set CFBundleShortVersionString '$(zim_get_version_from_module)'" \
  "$ZIM_APP_PLIST"
/usr/libexec/PlistBuddy -c "Set CFBundleVersion '$ZIM_BUILD'" "$ZIM_APP_PLIST"

# set copyright
/usr/libexec/PlistBuddy -c "Set NSHumanReadableCopyright 'Copyright © \
2008-2024 Jaap Karssenberg.'" "$ZIM_APP_PLIST"

# set app category
/usr/libexec/PlistBuddy -c \
  "Add LSApplicationCategoryType string 'public.app-category.utilities'" \
  "$ZIM_APP_PLIST"

# set folder access descriptions
/usr/libexec/PlistBuddy -c "Add NSDesktopFolderUsageDescription string \
'Zim needs your permission to access the Desktop folder.'" "$ZIM_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSDocumentsFolderUsageDescription string \
'Zim needs your permission to access the Documents folder.'" "$ZIM_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSDownloadsFolderUsageDescription string \
'Zim needs your permission to access the Downloads folder.'" "$ZIM_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSRemoveableVolumesUsageDescription string \
'Zim needs your permission to access removeable volumes.'" "$ZIM_APP_PLIST"

# add supported languages
/usr/libexec/PlistBuddy -c "Add CFBundleLocalizations array" "$ZIM_APP_PLIST"
for locale in "$SRC_DIR"/zim-desktop-wiki/translations/*.po; do
  if [ "$locale" = "en_GB" ]; then
    locale="en"
  fi
  /usr/libexec/PlistBuddy -c "Add CFBundleLocalizations: string \
'$(basename -s .po $locale)'" "$ZIM_APP_PLIST"
done

# add some metadata to make CI identifiable
if $CI; then
  for var in PROJECT_NAME PROJECT_URL COMMIT_BRANCH COMMIT_SHA \
    COMMIT_SHORT_SHA JOB_ID JOB_URL JOB_NAME PIPELINE_ID PIPELINE_URL; do
    # use awk to create camel case strings (e.g. PROJECT_NAME to ProjectName)
    /usr/libexec/PlistBuddy -c "Add CI$(
      echo $var | awk -F _ '{
        for (i=1; i<=NF; i++)
        printf "%s", toupper(substr($i,1,1)) tolower(substr($i,2))
      }'
    ) string $(eval echo \$CI_$var)" "$ZIM_APP_PLIST"
  done
fi

#------------------------------------------------- create GraphViz configuration

"$ZIM_APP_BIN_DIR"/dot -c

// SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <CoreFoundation/CoreFoundation.h>
#include <Python/Python.h>

#include <mach-o/dyld.h>

#include <string>
#include <sstream>
#include <vector>
#include <iostream>

#include "iniparser.hpp" // (c) 2015 by Borovik Alexey. Licensed under MIT.
                         // https://github.com/Lek-sys/LeksysINI

// https://developer.apple.com/library/archive/documentation/FileManagement/Conceptual/FileSystemProgrammingGuide/MacOSXDirectories/MacOSXDirectories.html
static const std::string BUNDLE_IDENTIFIER = "org.zim-wiki.Zim";
static const std::string SETTINGS_DIR = std::string(getenv("HOME")) +
                                        "/Library/Application Support/" +
                                        BUNDLE_IDENTIFIER;
static const std::string ZIM_INI = SETTINGS_DIR + "/zimapp.ini";

std::string get_executable_path()
{
  uint32_t size = PATH_MAX;
  char path[size];

  if (_NSGetExecutablePath(path, &size) == 0)
  {
    auto path_canonical = std::unique_ptr<char[]>(new char[size]);
    return std::string(realpath(path, path_canonical.get()));
  }

  return std::string();
}

std::string get_program_dir()
{
  if (auto executable_path = get_executable_path(); not executable_path.empty())
  {
    return executable_path.substr(0, executable_path.rfind("/"));
  }

  return std::string();
}

bool is_symlinked()
{
  uint32_t size = PATH_MAX;
  char path[size];
  char path_canonical[size];

  if (_NSGetExecutablePath(path, &size) == 0)
  {
    realpath(path, path_canonical);

    if (strcmp(path, path_canonical) != 0)
    {
      std::cout << "path           = " << path << std::endl
                << "path_canonical = " << path_canonical << std::endl
                << std::endl;
      return true;
    }
  }
  return false;
}

void setenv(const std::string &name, const std::string &value)
{
  setenv(name.c_str(), value.c_str(), 1);
}

static void setup_environment()
{
  std::string program_dir = get_program_dir();
  std::string contents_dir;
  contents_dir.assign(program_dir).append("/.."); // <TheApp.app>/Contents
  auto resources_dir = contents_dir + "/Resources";
  auto etc_dir = resources_dir + "/etc";
  auto bin_dir = resources_dir + "/bin";
  auto lib_dir = resources_dir + "/lib";
  auto share_dir = resources_dir + "/share";

  const std::string cache_dir =
      std::string(getenv("HOME")) + "/Library/Caches/" + BUNDLE_IDENTIFIER;

  // XDG
  // https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
  setenv("XDG_DATA_HOME", SETTINGS_DIR + "/share");
  setenv("XDG_DATA_DIRS", share_dir);
  setenv("XDG_CONFIG_HOME", SETTINGS_DIR);
  setenv("XDG_CACHE_HOME", cache_dir);
  setenv("XDG_RUNTIME_DIR", "/tmp"); // fallback, we don't have anything better

  // GTK
  // https://developer.gnome.org/gtk3/stable/gtk-running.html
  setenv("GTK_EXE_PREFIX", resources_dir);
  setenv("GTK_DATA_PREFIX", resources_dir);

  // GdkPixbuf
  // https://docs.gtk.org/gdk-pixbuf
  setenv("GDK_PIXBUF_MODULE_FILE",
         lib_dir + "/gdk-pixbuf-2.0/2.10.0/loaders.cache");

  // fontconfig
  setenv("FONTCONFIG_PATH", etc_dir + "/fonts");

  // GIO
  setenv("GIO_MODULE_DIR", lib_dir + "/gio/modules");

  // GObject Introspection Repository
  setenv("GI_TYPELIB_PATH", lib_dir + "/girepository-1.0");

  // gettext
  // This variable is actually only evaluated by gettext CLI tools, but because
  // of its familiarity we make Zim evaluate it to configure gettext.
  setenv("TEXTDOMAINDIR", share_dir + "/locale");

  // Python cache files (*.pyc)
  setenv("PYTHONPYCACHEPREFIX", cache_dir);

  // Python bindings for Enchant spellchecker
  setenv("PYENCHANT_LIBRARY_PATH", lib_dir + "/libenchant-2.2.dylib");

  // set PATH
  setenv("PATH", std::string(getenv("PATH")) + ":" + bin_dir);

  // set GUI language
  // https://www.gnu.org/software/gettext/manual/html_node/Locale-Environment-Variables.html
  const char *lang = getenv("LANG");
  if (lang == nullptr or std::string(lang).length() == 0)
  {
    CFLocaleRef cflocale = CFLocaleCopyCurrent();
    CFStringRef value =
        (CFStringRef)CFLocaleGetValue(cflocale, kCFLocaleIdentifier);
    char locale[32];
    CFStringGetCString(value, locale, 32, kCFStringEncodingUTF8);
    CFRelease(cflocale);
    setenv("LANG", std::string(locale).append(".UTF8"));
  }
  else
  {
    auto lang_no_hyphen = std::string(lang);
    if (size_t pos = lang_no_hyphen.find("UTF-8"); pos != std::string::npos)
    {
      lang_no_hyphen.replace(pos, 5, "UTF8");
    }
    setenv("LANG", lang_no_hyphen);
  }

  // an (optional) ini file can be used to set/override environment variables
  INI::File ini;
  if (ini.Load(SETTINGS_DIR + "/zimapp.ini"))
  {
    auto section = ini.GetSection("environment");
    if (section)
    {
      for (auto it = section->ValuesBegin(); it != section->ValuesEnd(); ++it)
      {
        setenv(it->first, it->second.AsString());
      }
    }
  }
}

size_t get_entry_count(const std::string &section_name)
{
  INI::File ini;

  if (ini.Load(ZIM_INI))
  {
    auto section = ini.GetSection(section_name);

    if (section)
      return section->ValuesSize();
    else
      return 0;
  }

  return 0;
}

int main(int argc, char *argv[])
{
  if (is_symlinked())
  {
    // While it is possible to get Meld somewhat working when symlinked
    // by manipulating 'argv[0]' with code like
    //
    //      char canonical_path[PATH_MAX];
    //      strcpy(canonical_path, get_executable_path().c_str());
    //      argv[0] = canonical_path;
    //
    // there are side effects. Some are visible (wrong window size, missing
    // icon in the dock), but who knows what else is getting messed up by this.
    // I haven't found any other app (including Apple's own apps) that supports
    // this. The main binary in an application bundle is simply not meant to be
    // symlinked to.
    std::cout
        << "You appear to be using a symlink to the Zim executable." << std::endl
        << "This is not supported." << std::endl;
    return 1;
  }

  //----------------------------------------------------------------------------

  int rc = 0;

  // copy all arguments into this vector
  auto arguments = std::vector<std::string>(argv, argv + argc);
  // prepend an additional argv[0] value
  arguments.insert(arguments.begin(), argv[0]);
  // append arguments from ini file
  if (INI::File ini; ini.Load(ZIM_INI))
  {
    if (auto section = ini.GetSection("arguments"); section)
    {
      for (auto it = section->ValuesBegin(); it != section->ValuesEnd(); ++it)
      {
        arguments.push_back(it->second.AsString());
      }
    }
  }
  // create new_argv that we can pass to PyConfig
  std::vector<const char *> new_argv(arguments.size());
  std::transform(arguments.begin(), arguments.end(), new_argv.begin(),
                 [](std::string &str)
                 { return str.c_str(); });

  setup_environment();

  PyStatus status;
  PyConfig config;
  PyConfig_InitPythonConfig(&config);
  status = PyConfig_SetBytesArgv(&config,
                                 new_argv.size(),
                                 const_cast<char **>(new_argv.data()));
  if (not PyStatus_Exception(status))
  {
    auto _fra_python_dir =
        (std::stringstream()
         << get_program_dir() << "/../Frameworks/Python.framework/Versions/"
         << PY_MAJOR_VERSION << "." << PY_MINOR_VERSION
         << "/lib/"
         << "python" << PY_MAJOR_VERSION << "." << PY_MINOR_VERSION)
            .str();
    wchar_t *fra_python_dir;
    PyConfig_SetBytesString(&config, &fra_python_dir, _fra_python_dir.c_str());
    PyWideStringList_Append(&config.module_search_paths, fra_python_dir);
    auto _fra_dynload_dir = _fra_python_dir + "/lib-dynload";
    wchar_t *fra_dynload_dir;
    PyConfig_SetBytesString(&config, &fra_dynload_dir, _fra_dynload_dir.c_str());
    PyWideStringList_Append(&config.module_search_paths, fra_dynload_dir);
    auto _site_dir =
        (std::stringstream()
         << get_program_dir() << "/../Resources/lib/"
         << "python" << PY_MAJOR_VERSION << "." << PY_MINOR_VERSION
         << "/site-packages")
            .str();
    wchar_t *site_dir;
    PyConfig_SetBytesString(&config, &site_dir, _site_dir.c_str());
    PyWideStringList_Append(&config.module_search_paths, site_dir);
    config.module_search_paths_set = 1;

    status = Py_InitializeFromConfig(&config);
    if (not PyStatus_Exception(status))
    {
      std::string filename = _site_dir + "/zim/zim";
      FILE *program_file = fopen(filename.c_str(), "r");
      rc = PyRun_SimpleFile(program_file, filename.c_str());
      fclose(program_file);
    }

    PyMem_RawFree(fra_python_dir);
    PyMem_RawFree(fra_dynload_dir);
    PyMem_RawFree(site_dir);
  }

  if (PyStatus_Exception(status))
  {
    Py_ExitStatusException(status);
  }

  PyConfig_Clear(&config);
  Py_Finalize();

  return rc;
}

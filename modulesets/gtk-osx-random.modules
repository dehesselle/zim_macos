<?xml version="1.0"?>
<!DOCTYPE moduleset SYSTEM "moduleset.dtd">
<?xml-stylesheet type="text/xsl" href="moduleset.xsl"?>
<moduleset>
  <!-- Please format this file using the following command:
       tidy -config tidy.conf -m gtk-osx-random.modules; sed -i "" '/^ *$/d' gtk-osx-random.modules
       You can get 'tidy' here: https://github.com/htacg/tidy-html5 -->
  <repository name="download.gnome.org"
              default="yes"
              href="https://download.gnome.org/sources/"
              type="tarball" />
  <repository name="github-tarball"
              href="https://github.com/"
              type="tarball" />
  <repository name="ftp.gnu.org"
              href="https://ftp.gnu.org/gnu/"
              type="tarball" />
  <repository name="iso-codes"
              href="https://salsa.debian.org/iso-codes-team/"
              type="git" />
  <repository name="graphviz"
              href="https://gitlab.com/graphviz/graphviz/-/archive/"
              type="tarball" />
  <repository name="sqlite"
              href="http://www.sqlite.org/"
              type="tarball" />
  <include href="../jhb/gtk-osx-random.modules" />
  <!-- TODO: update GraphViz -->
  <cmake id="graphviz"
         cmakeargs="-Dwith_gvedit=NO">
    <branch module="8.0.5/graphviz-8.0.5.tar.bz2"
            version="8.0.5"
            hash="sha256:c1901fe52483fad55fbf893ccd59a3dcaedd53f0d50b5aebbbf3deaba93b674d"
            repo="graphviz">
      <patch file="graphviz-remove-rpath.patch"
             strip="1" />
    </branch>
    <dependencies>
      <dep package="pango" />
      <dep package="librsvg" />
    </dependencies>
  </cmake>
  <!-- gtksourceview got a major version ahead:
       4.x.x supports recent versions of gtk+-3 and 5.x.x is for gtk4. -->
  <meson id="gtksourceview3"
         mesonargs="-Dvapi=false">
    <branch module="gtksourceview/4.8/gtksourceview-4.8.4.tar.xz"
            version="4.8.4"
            hash="sha256:7ec9d18fb283d1f84a3a3eff3b7a72b09a10c9c006597b3fbabbb5958420a87d">
    </branch>
    <dependencies>
      <dep package="gtk+-3.0" />
    </dependencies>
  </meson>
  <meson id="gsettings-desktop-schemas">
    <branch module="gsettings-desktop-schemas/46/gsettings-desktop-schemas-46.0.tar.xz"
            version="46.0"
            hash="sha256:493a46a1161b6388d57aa72f632a79ce96c42d5ffbd1d0b00f496ec5876f8575" />
    <dependencies>
      <dep package="gobject-introspection" />
    </dependencies>
  </meson>
  <!-- SQLite3 is also available from the macOS SDK -->
  <autotools id="sqlite"
             autogen-sh="configure"
             autogenargs="--disable-tcl --enable-threadsafe">
    <branch module="2024/sqlite-autoconf-3460000.tar.gz"
            version="3.46.0"
            hash="sha256:6f8e6a7b335273748816f9b3b62bbdc372a889de8782d7f048c653a447417a7d"
            repo="sqlite" />
  </autotools>
  <!---->
  <autotools id="hunspell"
             autogen-sh="autoreconf">
    <branch module="hunspell/hunspell/archive/v1.7.2.tar.gz"
            version="1.7.2"
            hash="sha256:69fa312d3586c988789266eaf7ffc9861d9f6396c31fc930a014d551b59bbd6e"
            checkoutdir="hunspell-1.7.2"
            rename-tarball="hunspell-1.7.2.tar.gz"
            repo="github-tarball" />
  </autotools>
  <!--
    Version 2.6.9 is the last before the Vala rewrite. Our custom patches need to be
    rewritten as well in order to upgrade beyond that.
  -->
  <autotools id="enchant"
             autogen-sh="configure"
             autogenargs="--enable-relocatable">
    <branch module="rrthomas/enchant/releases/download/v2.6.9/enchant-2.6.9.tar.gz"
            version="2.6.9"
            hash="sha256:d9a5a10dc9b38a43b3a0fa22c76ed6ebb7e09eb535aff62954afcdbd40efff6b"
            repo="github-tarball" >
      <patch file="enchant-relocatable.patch"
             strip="1" />
      <patch file="enchant-no_groff.patch"
             strip="1" />
    </branch>
    <dependencies>
      <dep package="glib" />
    </dependencies>
    <after>
      <dep package="hunspell" />
    </after>
  </autotools>
  <!-- iso-codes last released a tarball for version 4.1 in 2019 -->
  <autotools id="iso-codes"
             autogen-sh="configure">
    <branch module="iso-codes"
            repo="iso-codes"
            revision="v4.16.0" />
  </autotools>
  <autotools id="libxml2"
             autogen-sh="autoreconf"
             autogenargs='--libdir="$JHBUILD_LIBDIR" --with-python'>
    <branch module="libxml2/2.11/libxml2-2.11.5.tar.xz"
            version="2.11.5"
            hash="sha256:3727b078c360ec69fa869de14bd6f75d7ee8d36987b071e6928d4720a28df3a6">
    </branch>
    <after>
      <dep package="python3" />
    </after>
  </autotools>
</moduleset>

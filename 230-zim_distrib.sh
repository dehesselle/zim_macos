#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Create a disk image for distribution.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

### variables ##################################################################

SELF_DIR=$(dirname "$(greadlink -f "$0")")

### functions ##################################################################

# Nothing here.

### main #######################################################################

error_trace_enable

#------------------------------------------------------------- create disk image

convert \
  -size 440x404 canvas:transparent \
  -font /System/Library/Fonts/Monaco.ttf -pointsize 32 -fill black \
  -draw "text 90,55 'Zim $(zim_get_version_from_module)'" \
  -draw "text 165,172 '>>>'" \
  -pointsize 18 \
  -draw "text 90,80 'build $ZIM_BUILD'" \
  -fill red \
  -draw "text 40,275 'Unsigned development version!'" \
  -pointsize 14 \
  -draw "text 40,292 'xattr -r -d com.apple.quarantine Zim.app'" \
  "$SRC_DIR"/zim_dmg.png

dmgbuild_run "$SELF_DIR"/resources/zim_dmg.py "$ZIM_APP_PLIST"

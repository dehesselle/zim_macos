#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Install Zim to VER_DIR.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

### variables ##################################################################

# Nothing here.

### functions ##################################################################

# Nothing here.

### main #######################################################################

error_trace_enable

#------------------------------------------------------------------- install Zim

jhb build zim

# add build number to __version__ in AboutDialog
gsed -i "s/__version__/__version__ + ' ($ZIM_BUILD)'/" \
  "$LIB_DIR"/python*/site-packages/zim/gui/uiactions.py

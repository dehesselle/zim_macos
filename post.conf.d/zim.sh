# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# This file contains settings and functions related to packaging Zim.

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced
# shellcheck disable=SC2034 # no exports desired

### dependencies ###############################################################

# Nothing here.

### variables ##################################################################

ZIM_APP_DIR=$ART_DIR/Zim.app
ZIM_APP_CON_DIR=$ZIM_APP_DIR/Contents
ZIM_APP_RES_DIR=$ZIM_APP_CON_DIR/Resources
ZIM_APP_BIN_DIR=$ZIM_APP_RES_DIR/bin
ZIM_APP_SHR_DIR=$ZIM_APP_RES_DIR/share
ZIM_APP_LIB_DIR=$ZIM_APP_RES_DIR/lib
ZIM_APP_FRA_DIR=$ZIM_APP_CON_DIR/Frameworks
ZIM_APP_PLIST=$ZIM_APP_CON_DIR/Info.plist

ZIM_BUILD=${ZIM_BUILD:-0}

#----------------------------------------- Python runtime to be bundled with Zim

ZIM_PYTHON_VER_MAJOR=3
ZIM_PYTHON_VER_MINOR=10
ZIM_PYTHON_VER=$ZIM_PYTHON_VER_MAJOR.$ZIM_PYTHON_VER_MINOR
ZIM_PYTHON_URL="https://gitlab.com/api/v4/projects/26780227/packages/generic/\
python_macos/v21.1/python_${ZIM_PYTHON_VER/./}_$(uname -m).tar.xz"

#---------------------------------------- Python packages to be bundled with Zim

# https://pypi.org/project/numpy/
ZIM_PYTHON_PKG_NUMPY="\
  https://files.pythonhosted.org/packages/d8/8b/32dd9f08419023a4cf856c5ad0b4eba9b830da85eafdef841a104c4fc05a/numpy-2.2.1-cp310-cp310-macosx_11_0_arm64.whl\
  https://files.pythonhosted.org/packages/c7/c4/5588367dc9f91e1a813beb77de46ea8cab13f778e1b3a0e661ab031aba44/numpy-2.2.1-cp310-cp310-macosx_10_9_x86_64.whl\
"

# https://pypi.org/project/Pillow/
ZIM_PYTHON_PKG_PILLOW=pillow==11.0.0

# https://pypi.org/project/pycairo/
# https://pypi.org/project/PyGObject/
ZIM_PYTHON_PKG_PYGOBJECT="\
  pygobject==3.50.0\
  pycairo==1.27.0\
"

# https://pypi.org/project/pyenchant/
# https://pypi.org/project/pygtkspellcheck/
ZIM_PYTHON_PKG_PYGTKSPELLCHECK="\
  pyenchant==3.2.2\
  pygtkspellcheck==5.0.0\
  --no-deps\
"

# https://pypi.org/project/xdot/
ZIM_PYTHON_PKG_XDOT="\
  xdot==1.3\
  --no-deps\
"

### functions ##################################################################

function zim_get_version_from_module
{
  xmllint \
    --xpath "string(//moduleset/distutils[@id='zim']/branch/@version)" \
    "$ETC_DIR"/modulesets/zim/zim.modules \
    2>/dev/null
}

function zim_pipinstall
{
  local packages=$1     # name of variable that resolves to list of packages
  local options=$2      # optional

  # turn package names into filenames of our wheels
  local wheels
  for package in $(eval echo \$"$packages"); do
    if [ "${package::8}" = "https://" ]; then
      if [[ $package != *$(uname -m).whl ]]; then
        continue # skip package for different architecture
      fi
      package=$(basename "$package")
    elif [ "${package:1:1}" = "-" ]; then
      options="$options $package"
      continue
    else
      package=$(eval echo "${package/==/-}"*.whl)
    fi

    wheels="$wheels $PKG_DIR/$package"
  done

  (
    # shellcheck disable=SC2030 # local PATH modification is intentional
    export PATH=$ZIM_APP_FRA_DIR/Python.framework/Versions/Current/bin:$PATH

    # shellcheck disable=SC2086 # we need word splitting here
    pip3 install \
      --prefix "$ZIM_APP_RES_DIR" \
      $options \
      $wheels
  )

  local zim_pipinstall_func
  zim_pipinstall_func=zim_pipinstall_$(echo "${packages##*_}" |
    tr "[:upper:]" "[:lower:]")

  if declare -F "$zim_pipinstall_func" > /dev/null; then
    $zim_pipinstall_func
  fi
}

function zim_pipinstall_numpy
{
  local numpy_dir=$ZIM_APP_LIB_DIR/python$ZIM_PYTHON_VER/site-packages/numpy

  find "$numpy_dir" '(' -name "*.so" -o -name "*.dylib" ')' \
    -exec codesign --remove-signature {} \;

  find "$numpy_dir" -name "*.a" -delete

  # remove libs intended for other architectures
  for lib in "$numpy_dir"/.dylibs/*.dylib; do
    if ! file "$lib" | grep "$(uname -m)"; then
      rm "$lib"
    fi
  done

  # remove CLI tools
  rm "$ZIM_APP_RES_DIR"/bin/f2py*
}

function zim_pipinstall_pillow
{
  lib_change_paths \
    @loader_path/../../.. \
    "$ZIM_APP_LIB_DIR" \
    "$ZIM_APP_LIB_DIR"/python$ZIM_PYTHON_VER/site-packages/PIL/*.so
}

function zim_pipinstall_pygobject
{
  # GObject Introspection
  lib_change_paths \
    @loader_path/../../.. \
    "$ZIM_APP_LIB_DIR" \
    "$ZIM_APP_LIB_DIR"/python$ZIM_PYTHON_VER/site-packages/gi/_gi*.so

  # Cairo
  lib_change_paths \
    @loader_path/../../.. \
    "$ZIM_APP_LIB_DIR" \
    "$ZIM_APP_LIB_DIR"/python$ZIM_PYTHON_VER/site-packages/cairo/\
_cairo.cpython-${ZIM_PYTHON_VER/./}-darwin.so
}

function zim_pipinstall_xdot
{
  ln -s dot "$ZIM_APP_BIN_DIR"/fdp
  rm "$ZIM_APP_BIN_DIR"/xdot
}

function zim_download_python
{
  curl -o "$PKG_DIR"/"$(basename "${ZIM_PYTHON_URL%\?*}")" -L "$ZIM_PYTHON_URL"
}

function zim_install_python
{
  local target_dir=$1

  target_dir=${target_dir:-$ZIM_APP_FRA_DIR}

  mkdir -p "$target_dir"
  tar -C "$target_dir" -xf "$PKG_DIR"/"$(basename "${ZIM_PYTHON_URL%\?*}")"
  local python_lib=Python.framework/Versions/$ZIM_PYTHON_VER/Python
  install_name_tool -id @executable_path/../Frameworks/$python_lib \
    "$target_dir"/$python_lib
}

function zim_uninstall_python
{
  local target_dir=$1

  rm -rf "${target_dir:?}/Python.framework"
}

function zim_build_wheels
{
  # create a venv based on Python.framework
  local tmp_dir=$TMP_DIR/${FUNCNAME[0]}
  zim_install_python "$tmp_dir"
  "$tmp_dir"/Python.framework/Versions/Current/bin/python3 \
      -m venv "$tmp_dir"/venv

  (
    if [ "$(uname -m)" = "x86_64" ]; then
      # needs to match Python.framework
      export MACOSX_DEPLOYMENT_TARGET=10.13
    else
      export MACOSX_DEPLOYMENT_TARGET=$SYS_SDK_VER
    fi

    # shellcheck disable=SC1091 # cannot follow dynmaically created location
    source "$tmp_dir"/venv/bin/activate
    pip3 install wheel==0.41.2

    for package_set in ${!ZIM_PYTHON_PKG_*}; do
      local packages
      local options
      for package in $(eval echo \$"$package_set"); do
        if [ "${package::8}" = "https://" ]; then
          if [[ $package != *$(uname -m).whl ]]; then
            continue # skip package for different architecture
          fi
          pip3 download -d "$PKG_DIR" "$package"
        elif [ "${package:1:1}" = "-" ]; then
          options="$options $package"
        else
          packages="$packages $package"
        fi
      done

      if [ -n "$packages" ]; then
        # shellcheck disable=SC2086 # we need word splitting here
        pip3 wheel \
          --no-binary :all: \
          --use-feature=no-binary-enable-wheel-cache \
          -w "$PKG_DIR" \
          -f "$PKG_DIR" \
          $options \
          $packages
        packages=""
      fi
    done

    # Exclude wheels from cleanup procedure.
    find "$PKG_DIR" -type f -name '*.whl' \
      -exec bash -c 'basename "$1" >> "${2:?}"/.keep' _ {} "$PKG_DIR" \;
  )

  rm -rf "${tmp_dir:?}"
}

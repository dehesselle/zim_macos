<?xml version="1.0" standalone="no"?> <!--*- mode: xml -*-->
<app-bundle>

  <meta>
    <!-- Where to pick up the GTK+ installation, icon themes,
         etc. Note that "${env:JHBUILD_PREFIX}" is evaluated to the
         value of the environment variable JHBUILD_PREFIX. You can
         define additional prefixes and refer to them in paths
         throughout this file on the form "${prefix:name}". This is
         useful for installing certain libraries or even the
         application itself separately. Note that JHBUILD_PREFIX is
         defined by jhbuild, so it you are not using jhbuild you can
         either define your own or just hardcode the path here.
    -->
    <prefix name="default">${env:JHBUILD_PREFIX}</prefix>

    <!-- The project directory is the default location of the created
         app. If you leave out the path, the current directory is
         used. Note the usage of an environment variable here again.
    -->
    <destination overwrite="yes">${env:ART_DIR}</destination>

    <!-- Comment this out to keep the install names in binaries.  -->
    <run-install-name-tool/>

     <!-- Indicate the active gtk version to use. This is needed only
          for gtk+-3.0 projects.
     -->
    <gtk>gtk+-3.0</gtk>
  </meta>

  <!-- The special macro "${project}" refers to the directory where
       this bundle file is located. The application name and bundle
       identifier are taken from the plist file.
  -->
  <plist>${project}/../resources/zim.plist</plist>

  <!-- This is mandatory, and the result gets named to the appname from
       Info.plist.
  -->
  <main-binary>
    ${prefix}/bin/zimlauncher
  </main-binary>

  <!-- Gtk+ is required by python modules which aren't searched for
       dependencies, so we have to pull it in explicitly. It will
       bring Pango and Cairo with it. Note the use of ${gtkversion},
       which expands to either 2.0 or 3.0, depending on which value of
       gtk is specified.-->
  <binary>
    ${prefix}/lib/libgtkmacintegration-gtk3.dylib
  </binary>

  <!-- Copy in GTK+ modules. Note the ${gtkdir} macro, which expands
       to the correct library subdirectory for the specified gtk
       version.
  -->
  <binary>
     ${prefix}/lib/${gtkdir}/3.0.0/immodules/*.so
  </binary>
  <binary>
    ${prefix}/lib/${gtkdir}/${pkg:${gtk}:gtk_binary_version}/printbackends/*.so
  </binary>

  <!-- Starting with 2.24, gdk-pixbuf installs into its own directory. -->
  <binary>
    ${prefix}/lib/gdk-pixbuf-2.0/${pkg:gdk-pixbuf-2.0:gdk_pixbuf_binary_version}/loaders/*.so
  </binary>

  <binary>
    ${prefix}/lib/libgirepository-1.0.1.dylib
  </binary>
  <gir>
    ${prefix}/share/gir-1.0/*.gir
  </gir>
  <binary>
    ${prefix}/lib/libgtksourceview-4.0.dylib
  </binary>
  <binary>
    ${prefix}/lib/libenchant-2.2.dylib
  </binary>
  <binary>
    ${prefix}/lib/enchant-2/*.so
  </binary>
  <binary>
    ${prefix}/lib/libgv*.dylib
  </binary>
  <binary>
    ${prefix}/lib/graphviz/*.dylib
  </binary>
  <binary>
    ${prefix}/lib/libreadline.8.dylib
  </binary>
  <binary>
    ${prefix}/bin/dot
  </binary>
  <!-- WebP Codec -->
  <binary>
    ${prefix}/lib/libwebp.7.dylib
  </binary>
  <binary>
    ${prefix}/lib/libwebpmux.3.dylib
  </binary>
  <binary>
    ${prefix}/lib/libwebpdemux.2.dylib
  </binary>
  <!-- Translation filenames, one for each program or library that you
       want to copy in to the bundle. The "dest" attribute is
       optional, as usual. Bundler will find all translations of that
       library/program under the indicated directory and copy them.
  -->
  <translations name="gtk30">
    ${prefix}/share/locale
  </translations>
  <translations name="zim">
    ${prefix}/share/locale
  </translations>
  <translations name="gtksourceview-4">
    ${prefix}/share/locale
  </translations>
  <translations name="gtk-mac-integration">
    ${prefix}/share/locale
  </translations>

  <!-- We have to pull in the python modules, which are mixed python
       and loadable modules.
  -->
  <data>
    ${prefix}/lib/python3.10/
  </data>

  <!-- additional data -->
  <data>
    ${prefix}/share/zim
  </data>
  <data>
    ${prefix}/share/gtksourceview-4
  </data>
  <data dest="${bundle}/Contents/Resources/share/icons/hicolor">
    ${prefix}/usr/src/zim-desktop-wiki/xdg/hicolor
  </data>
  <data>
    ${prefix}/share/enchant-2
  </data>
  <data>
    ${prefix}/share/graphviz
  </data>
  <data>
    ${prefix}/share/glib-2.0
  </data>

  <data dest="${bundle}/Contents/Resources/share/applications/DefaultTextEditor.desktop">
    ${project}/../resources/DefaultTextEditor.desktop
  </data>
  <data dest="${bundle}/Contents/Resources/share/applications/defaults.list">
    ${project}/../resources/defaults.list
  </data>
  <data dest="${bundle}/Contents/Resources/etc/gtk-3.0/gtk.css">
    ${project}/../resources/gtk.css
  </data>

  <!-- Copy in the themes data. You may want to trim this to save space
       in your bundle.
  -->
  <data>
    ${prefix}/share/themes
  </data>

  <!-- Copy icons. Note that the .icns file is an Apple format which
       contains up to 4 sizes of icon. You can use
       /Developer/Applications/Utilities/Icon Composer.app to import
       artwork and create the file.
  -->
  <data dest="${bundle}/Contents/Resources/Zim.icns">
    ${project}/../resources/zim_dev.icns
  </data>
  <data dest="${bundle}/Contents/Resources">
    ${project}/../resources/Notebook.icns
  </data>

  <!-- Icon themes to copy. The "icons" property can be either of
       "auto", "all", or "none". All or none should be
       self-explanatory, while auto means that the script will try to
       figure out which icons are needed. This is done by getting all
       the strings from all copied binaries, and matching them against
       icon names. To be safe, you should use "all". "none" is useful
       if you want just the index.theme file but no icons, mostly
       needed for the "hicolor" base theme.
  -->
  <icon-theme icons="all">
    Adwaita
  </icon-theme>
  <icon-theme icons="all">
    hicolor
  </icon-theme>

</app-bundle>

# Zim for macOS

![master branch](https://gitlab.com/dehesselle/zim_macos/badges/master/pipeline.svg?key_text=master)
![latest release](https://gitlab.com/dehesselle/zim_macos/-/badges/release.svg?key_text=latest%20release&key_width=100&value_width=100)

![zim](resources/zim.png)

This is the official macOS release for [Zim](https://zim-wiki.org).

## Installation

Downloads are available in the [Releases](https://gitlab.com/dehesselle/zim_macos/-/releases) section.

The app is standalone, relocatable and supports macOS Big Sur up to macOS Sequoia.

### data locations

| type | path |
| --- | --- |
| configuration files | `~/Library/Application Support/org.zim-wiki.Zim` |
| cache files | `~/Library/Caches/org.zim-wiki.Zim` |
| runtime files | `/tmp` |

### advanced configuration

💁 _Incorrect usage of the following can break the app - you have been warned!_

Specific to the macOS app, you can create a file named `zimapp.ini` in the configuration files folder. You can modify the environment with the help of this file.

A common use case is to modify `PATH` so Zim can pick up dependencies that don't come bundled with the app. For example, to make the equation editor find Latex that you installed with Homebrew, add the following:

```ini
[environment]
# Important:
#   - replace </path/to/homebrew/bin> accordingly
#   - replace </default/path> with the output from 'echo $PATH'
#   - always have the app's bin folder at the end
PATH=</path/to/homebrew/bin>:</default/path>:/Applications/Zim.app/Contents/Resources/bin
```

It is important to understand that

- setting an environment variable overrides any default/automatic configuration of that variable. (This is why you can easily break things here.)
- there is no variable resolution. (This is not a shell.)

## Contact

Please use the official channels:

- report issues to Zim's [issue tracker](https://github.com/zim-desktop-wiki/zim-desktop-wiki/issues)
- ask questions in Zim's [Discussions](https://github.com/zim-desktop-wiki/zim-desktop-wiki/discussions)

Check the [contributing guidelines](https://github.com/zim-desktop-wiki/zim-desktop-wiki/blob/develop/CONTRIBUTING.md) before posting!

## Acknowledgements

- [gtk-osx](https://gitlab.gnome.org/GNOME/gtk-osx) for building GTK with JHBuild.
- [Leksys' INIParser](https://github.com/Lek-sys/LeksysINI) for an easy-to-use INI parser.

## License

This work is licensed under [GPL-2.0-or-later](LICENSE).  
Zim is licensed under [GPL-2.0-or-later](https://github.com/zim-desktop-wiki/zim-desktop-wiki#copyright-and-license).
